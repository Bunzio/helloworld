package elenx.net.HelloWorld;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Created by Bunzio on 01.04.2018
 * Contact: bunzeladam@gmail.com
 */
@Component
public class Hi implements CommandLineRunner {
    @java.lang.Override
    public void run(java.lang.String... args) throws Exception {
        System.out.println("Hello World!!!");

    }
}
